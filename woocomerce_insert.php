<?php

/**
 * Created by PhpStorm.
 * User: Ayon
 * Date: 20-May-18
 * Time: 2:43 PM
 */
require __DIR__ . '/vendor/autoload.php';


use Automattic\WooCommerce\Client;
require 'taking_data.php';
use ayon\taking_data;


class woocomerce_insert
{
function woocomerce_input(){
    $aa = new ayon\taking_data();


    $woocommercecloverdata = $aa->data_fetch();

    $woocommerce = new Client(
        'https://www.liquidationsplus.com',
        'ck_a46ff3e644e857b842abab259ba4d714962b59e3',
        'cs_aa1abad671602c21eb685a31db8b21e2eb2718fb',
        [
            'wp_api' => true,
            'version' => 'wc/v2',
        ]
    );



    $product_data = [
        'name' => $woocommercecloverdata['name'],
        'type' => $woocommercecloverdata['type'],
        'regular_price'=>$woocommercecloverdata['regular_price'],
        'description' => $woocommercecloverdata['description'],
        'short_description' => $woocommercecloverdata['short_description'],

        'categories' => array()
        /*[
             [
                 'id' => 17
             ],
         [
             'id' => 18
         ],

         ]*/,
        'images' => [
            [
                'src' => $woocommercecloverdata['images'],
                'position' => 0
            ],
            /*  [
                  'src' => '',
                  'position' => 1
              ],
              [
                  'src' => '',
                  'position' => 2
              ],
              [
                  'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_3_back.jpg',
                  'position' => 3
              ]*/
        ],
        'attributes' => [
            [
                'id' => 7,
                'name' => 'Condition',
                'position' => 0,
                'visible' => true,
                'order_by' => 'menu_order',
                'has_archives' => True,
                'type' => 'select',
                'variation' => false,
                'options' => [
                    $woocommercecloverdata['condition'],
                ]
            ],
            [
                'id' => 8,
                'name' => 'Lot Size',
                'position' => 0,
                'visible' => true,
                'order_by' => 'menu_order',
                'has_archives' => True,
                'type' => 'select',
                'variation' => false,
                'options' => [
                    $woocommercecloverdata['lot_size'],
                ]
            ]
        ],
        'default_attributes' => [
            [
                'id' => 7,
                'option' => $woocommercecloverdata['condition']
            ],
            [
                'id' => 8,
                'option' => $woocommercecloverdata['lot_size']
            ]
        ]
    ];



    /*for ($i=0;$i<$category_count;$i++){
        $product_data['categories'][]=array(
          'id'=>$woocommercecloverdata['categories'][$i]

        );

    }*/
    foreach ($woocommercecloverdata['categories'] as $user) {
        $product_data['categories'][] = array(
            'id' => $user,

        );
    }

//print_r($product_data);
//die();

    $woocommerce->post('products', $product_data);

    /*
    try {
        // Array of response results.
        $results = $woocommerce->get('products');
        // Example: ['customers' => [[ 'id' => 8, 'created_at' => '2015-05-06T17:43:51Z', 'email' => ...

        // Last request data.
        $lastRequest = $woocommerce->http->getRequest();
        $lastRequest->getUrl(); // Requested URL (string).
        $lastRequest->getMethod(); // Request method (string).
        $lastRequest->getParameters(); // Request parameters (array).
        $lastRequest->getHeaders(); // Request headers (array).
        $lastRequest->getBody(); // Request body (JSON).
    //print_r($lastRequest);
        // Last response data.
        $lastResponse = $woocommerce->http->getResponse();
        $lastResponse->getCode(); // Response code (int).
        $lastResponse->getHeaders(); // Response headers (array).
        $lastResponse->getBody(); // Response body (JSON).
    //print_r($lastResponse);
    } catch (HttpClientException $e) {
        $e->getMessage(); // Error message.
        $e->getRequest(); // Last request data.
        $e->getResponse(); // Last response data.
    }*/




}
}

$aaa=new woocomerce_insert();
print_r($aaa->woocomerce_input());